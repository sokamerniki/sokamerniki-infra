# Sokamerniki-infra



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sokamerniki/sokamerniki-infra.git
git branch -M main
git push -uf origin main
```
## Usage
- Ensure you have the necessary prerequisites mentioned above.
- Set values for variables in variables.tf according to your requirements.
- Run terraform init to initialize the Terraform configuration.
- Run terraform apply to apply the configuration and deploy the infrastructure.
- After deployment, review the outputs for relevant information.

# main.tf

## Resource Group (azurerm_resource_group)
```
resource "azurerm_resource_group" "devops-rg" {
  name     = "devops-resources"
  location = var.resource_location
}
```

- Creates an Azure resource group named "devops-resources" in the specified location.

## Virtual Network (azurerm_virtual_network)
```
resource "azurerm_virtual_network" "devops-vn" {
  name     = "devops-network"
  location = azurerm_resource_group.devops-rg.location


  resource_group_name = azurerm_resource_group.devops-rg.name
  address_space       = ["10.0.0.0/16"]
  tags = {
    environment = "dev"
  }
}
```

- Creates an Azure virtual network named "devops-network" with an address space of "10.0.0.0/16".


## Subnet (azurerm_subnet)

```
resource "azurerm_subnet" "devops-subnet" {
  name                 = "devops-subnet"
  resource_group_name  = azurerm_resource_group.devops-rg.name
  virtual_network_name = azurerm_virtual_network.devops-vn.name
  address_prefixes     = ["10.0.2.0/24"]
}
```

- Creates a subnet named "devops-subnet" with an address prefix of "10.0.2.0/24" within the virtual network.

## Network Security Group (azurerm_network_security_group)
```
resource "azurerm_network_security_group" "devops-sg" {
  name                = "devops-sg"
  location            = azurerm_resource_group.devops-rg.location
  resource_group_name = azurerm_resource_group.devops-rg.name


  tags = {
    environment = "dev"
  }
}
```

- Creates a network security group named "devops-sg" with an inbound rule allowing all traffic.

## Security Rule (azurerm_network_security_rule)
```
resource "azurerm_network_security_rule" "devops-dev-rule" {
  name                        = "devops-dev-rule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.devops-rg.name
  network_security_group_name = azurerm_network_security_group.devops-sg.name
}
```

- Creates a security rule named "devops-dev-rule" allowing all inbound traffic.

## Subnet-Network Security Group Association (azurerm_subnet_network_security_group_association)
```
resource "azurerm_subnet_network_security_group_association" "devops-sga" {
  subnet_id                 = azurerm_subnet.devops-subnet.id
  network_security_group_id = azurerm_network_security_group.devops-sg.id


  depends_on = [azurerm_subnet.devops-subnet]
}

```

- Associates the subnet with the created network security group.

## Public IP Address (azurerm_public_ip)
```
resource "azurerm_public_ip" "devops-ip" {
  name                = "devops-ip"
  resource_group_name = azurerm_resource_group.devops-rg.name
  location            = azurerm_resource_group.devops-rg.location
  allocation_method   = "Dynamic"


  tags = {
    environment = "dev"
  }
}


```

- Creates a dynamic public IP address named "devops-ip".

## Network Interface (azurerm_network_interface)
```
resource "azurerm_network_interface" "devops-nic" {
  name                = "devops-nic"
  location            = azurerm_resource_group.devops-rg.location
  resource_group_name = azurerm_resource_group.devops-rg.name




  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.devops-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.devops-ip.id


  }
  tags = {
    environment = "dev"
  }
}
```

- Creates a network interface named "devops-nic" with a dynamic private IP address and associated public IP.

## Linux Virtual Machine (azurerm_linux_virtual_machine)

```
resource "azurerm_linux_virtual_machine" "dev-vm" {
  name                = "dev-vm"
  resource_group_name = azurerm_resource_group.devops-rg.name
  location            = azurerm_resource_group.devops-rg.location
  size                = var.machine_size
  admin_username      = var.admin_username
  network_interface_ids = [
    azurerm_network_interface.devops-nic.id
  ]


  admin_ssh_key {
    username   = var.admin_username
    public_key = file(var.ssh_public_key_path)
  }


  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }


  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
  tags = {
    environment = "dev"
  }
}

```

- Deploys an Ubuntu Linux virtual machine named "dev-vm" with specified configurations.


## Nomad Deployer Module (module "nomad_deployer")
```
module "nomad_deployer" {
  source = "./modules/nomad-deployer"
  
  vm_id  = azurerm_linux_virtual_machine.dev-vm.id
  vm_ip  = azurerm_linux_virtual_machine.dev-vm.public_ip_address
}
```

- Uses a custom module to deploy a Nomad server with the specified VM ID and IP.

## Runner Registration Module (module "runner-registration")
```
module "runner-registration" {
  source = "./modules/runner-registration"

  vm_id = azurerm_linux_virtual_machine.dev-vm.id
  vm_ip = azurerm_linux_virtual_machine.dev-vm.public_ip_address

}
```

- Uses a custom module to register a GitLab Runner with the specified VM ID and IP.

## Modules
## Nomad Deployer Module (./modules/nomad-deployer):
- This module deploys a Nomad server on the specified VM.
## Runner Registration Module (./modules/runner-registration):
- This module registers a GitLab Runner on the specified VM.
