# Traefik Integration with Nomad using GitLab CI

This repository contains configuration files to deploy Traefik, a modern reverse proxy and load balancer, on HashiCorp Nomad using GitLab CI. The setup allows for easy management and dynamic configuration of Traefik for your applications.

## Prerequisites

Before deploying Traefik with Nomad, make sure you have the following components installed and configured:

- HashiCorp Nomad
- Docker (used as the driver for Nomad tasks)
- GitLab CI configured for your project

## Configuration Files

### Traefik Nomad Job Configuration (`traefik.nomad`)

The `traefik.nomad` file defines a Nomad job that launches Traefik as a service. It includes the necessary configurations for Docker, resource allocation, and dynamic configuration through templates.

```hcl
job "traefik" {
  # ... (existing configuration)

  group "traefik" {
    count = 1

    task "traefik" {
      driver = "docker"

      config {
        image = "traefik:v2.5"
        ports = ["80:80", "8080:8080"]

        volumes = [
          "/traefikfolder/config:/etc/traefik",
        ]
      }

      template {
        data = <<EOF
          # Additional dynamic configuration if needed
        EOF

        destination = "local/config/dynamic.toml"
      }

      resources {
        cpu    = 500 # adjust as needed
        memory = 256 # adjust as needed
      }
    }
  }
}
```

Adjust the resource allocation as per your system requirements.

### GitLab CI Configuration (`.gitlab-ci.yml`)

The `.gitlab-ci.yml` file contains a single deploy stage that runs the Nomad job to deploy Traefik.

```yaml
stages:
  - deploy

deploy:
  stage: deploy
  script:
    - nomad job run traefik.nomad
```

This stage is triggered when changes are pushed to the repository.

## Usage

1. Ensure that Nomad and Docker are installed and properly configured.
2. Customize the Traefik configuration in `traefik.nomad` to meet your specific requirements.
3. Commit and push changes to your GitLab repository.
4. GitLab CI will automatically trigger the deployment stage, running the Nomad job to deploy Traefik.

## Additional Notes

- Adjust the CPU and memory resources in the Nomad job configuration based on your server specifications.
- For dynamic Traefik configurations, modify the template section in the Nomad job accordingly.

Feel free to explore Traefik's documentation for advanced configurations and optimizations: [Traefik Documentation](https://doc.traefik.io/traefik/).