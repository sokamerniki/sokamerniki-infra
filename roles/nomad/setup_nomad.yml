---
- name: Install and Configure HashiCorp Nomad
  hosts: all
  become: yes

  tasks:
    - name: Install Docker Engine
      apt:
        name: docker.io
        state: present
        
    - name: Install Docker CLI
      apt:
        name: docker-compose
        state: present

    - name: Start Docker service
      service:
        name: docker
        state: started
        enabled: true

    - name: Install necessary packages
      apt:
        name:
          - unzip
          - curl
        state: latest
      when: ansible_os_family == "Debian"

    - name: Download Nomad
      get_url:
        url: "https://releases.hashicorp.com/nomad/1.7.2/nomad_1.7.2_linux_amd64.zip"
        dest: "/tmp/nomad.zip"
        mode: '0644'

    - name: Unzip Nomad
      unarchive:
        src: "/tmp/nomad.zip"
        dest: "/usr/local/bin"
        remote_src: yes

    - name: Create Nomad directories
      file:
        path: "{{ item }}"
        state: directory
        mode: '0755'
      with_items:
        - /etc/nomad.d
        - /var/lib/nomad

    - name: Add Nomad configuration file
      template:
        src: nomad-config.hcl.j2
        dest: /etc/nomad.d/config.hcl
      notify: Restart Nomad

    - name: Create Nomad systemd service file
      template:
        src: nomad.service.j2
        dest: /etc/systemd/system/nomad.service
      notify: Restart Nomad

    - name: Enable and start Nomad service
      systemd:
        name: nomad
        enabled: yes
        state: started
        daemon_reload: yes

  handlers:
    - name: Restart Nomad
      systemd:
        name: nomad
        state: restarted
        daemon_reload: yes

  vars:
      ansible_ssh_private_key_file: ~/.ssh/id_rsa