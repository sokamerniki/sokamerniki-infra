# Assignment6

Team members:
- 21B030723 Toleugaliyev Nurdaulet
- 21B030915 Sarshaev Erkhan
- 21B030662 Glazhdin Sabir
- 21B030903 Rymkul Yerassyl
- 21B031212 Bauyrzhan Kilybai
- 21B030667 Zhagypar Gabdylgaziz

## Getting started

This Terraform configuration consists of two main files: main.tf and repositories.tf. The former sets up a Google Cloud Platform (GCP) virtual machine instance and installs GitLab on it, while the latter creates three GitLab projects. Let's go through each file and understand its purpose and usage. [Use the template at the bottom](#editing-this-readme)!

## Usage

- Ensure that Terraform is installed on your system.
- Replace placeholders and configure the necessary variables in variables.tf file.
- Run ```terraform init``` to initialize your Terraform configuration.
- Execute ```terraform apply``` to create the resources defined in the configuration.
- Review the output for any errors and confirm to apply the changes.
- Terraform will provision the GCP VM instance and GitLab projects.

# main.tf

## Terraform Block

```
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.88.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      # version = "1.2.3"  
    }
  }
}
```

- In this block, we specify the required Terraform providers for Google Cloud Platform and GitLab.

## Google Cloud Platform Provider
```
provider "google" {
  credentials = file("creds.json")
  project     = var.gcp_project
  region      = "asia-east2"
}
```

- This section configures the Google Cloud Platform provider with authentication credentials from the "creds.json" file, the GCP project name (var.gcp_project), and the region ("asia-east2").

## GitLab Provider

```
provider "gitlab" {
  token = var.gitlab_token
}
```

- Here, the GitLab provider is configured with an access token from a variable (var.gitlab_token).

## Google Compute Instance Resource

```
resource "google_compute_instance" "vm" {
  # some code
}
```

- This block defines a Google Compute Engine instance named "vm" with specific configuration settings.

## Google Compute Instance Configuration

```
name         = var.instance_name
machine_type = "n1-standard-1"
zone         = "asia-east2-b"
```

- These settings specify the instance name, machine type, and zone for the Google Compute Engine instance.

## Boot Disk Configuration
```
boot_disk {
  initialize_params {
    image = "debian-cloud/debian-10"
  }
}
```
- This block configures the boot disk of the instance with the Debian 10 image.

## Network Configuration
```
network_interface {
  network = "default"
  access_config {}
}
```
- The network interface is configured to use the default network and allows access configuration.

## Metadata and Startup Script
```
metadata_startup_script = <<-SCRIPT
  # ... (startup script)
SCRIPT
```
- This multiline string contains a startup script that configures the VM by adding a user, installing packages, and setting up GitLab.

## Metadata SSH Keys
```
metadata = {
  ssh-keys = "ansible:${file("~/.ssh/id_rsa.pub")}"
}
```
An SSH key is added to the VM's metadata, allowing the specified user (ansible) to access the instance.

## Output

```
output "instance_ip_address" {
  value       = google_compute_instance.vm-v1.network_interface[0].access_config[0].nat_ip
  description = "The external IP address of the VM"
}
```

- An output block provides the external IP address of the created VM instance.

## Health checking

```
ping_result=$(ping -c 1 $HOST)

if [[ $ping_result == *"1 packets transmitted, 1 received"* ]]; then
    echo "The server with IP $HOST is up and running"
else
    echo "The server with IP $HOST is down"
    exit 1
fi
}
```

- We ping the server, if it's server is up and running, our ping will work and we will get output like "The server with IP is up and running"

```
if ssh -q -o ConnectTimeout=5 -o StrictHostKeyChecking=no "$USER@$HOST" exit; then
   echo "Server is reachable."
else
   echo "Server is not reachable."
fi
```

- And additionally, we connect via ssh to check if the server is available for connection

- An example of successful healthchecking. [Screenshot](https://drive.google.com/file/d/1fO32yi1W1mLrDq6yqBDO2RIM37sdv2wX/view?usp=sharing)
