resource "null_resource" "install_nomad" {

  triggers = {
    vm_id = var.vm_id
  }

  connection {
    type        = "ssh"
    host        = var.vm_ip
    user        = var.admin_username
    private_key = file(var.ssh_private_key_path)
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y unzip",
      "curl -o nomad.zip https://releases.hashicorp.com/nomad/1.7.2/nomad_1.7.2_linux_amd64.zip",
      "unzip nomad.zip",
      "sudo mv nomad /usr/local/bin/",
      "sudo chmod +x /usr/local/bin/nomad",
    ]
  }
}