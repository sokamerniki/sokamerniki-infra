variable "admin_username" {
  description = "VM admins username"
  type        = string
  default     = "sokamernik"

}
variable "ssh_public_key_path" {
  description = "SSH public key"
  type        = string
  default     = "~/.ssh/devopsazurekey.pub"

}
variable "ssh_private_key_path" {
  description = "SSH public key"
  type        = string
  default     = "~/.ssh/devopsazurekey"

}
variable "gitlab_group_id" {
  description = "Gitlab Group ID Credentials"
  type        = string
  default     = "73894190"

}

variable "vm_id" {
  description = "VMs ID"
  type = string
}

variable "vm_ip" {
  description = "VMs public IP"
  type = string
  
}

