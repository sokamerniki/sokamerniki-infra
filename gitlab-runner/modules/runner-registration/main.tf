
resource "null_resource" "install_gitlab_runner" {
  triggers = {
    vm_id = var.vm_id
  }

  connection {
    type        = "ssh"
    host        = var.vm_ip
    user        = var.admin_username
    private_key = file(var.ssh_private_key_path)
  }

  provisioner "remote-exec" {
    inline = [
      "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash",
      "sudo apt-get install gitlab-runner",
    ]
  }
}

resource "gitlab_user_runner" "base_runner" {
  runner_type  = "group_type"
  access_level = "not_protected"
  description  = "Sokamerniki test group runner"
  group_id     = var.gitlab_group_id
  locked       = false
  paused       = false
  untagged     = true
  tag_list     = ["ubuntu", "shell", "linux"]

}

resource "local_file" "config" {
  filename = "${path.module}/config.toml"
  content  = <<CONTENT
  concurrent = 1

  [[runners]]
    name = "Hello Terraform"
    url = "https://gitlab.com/"
    id = ${gitlab_user_runner.base_runner.id}
    token = "${gitlab_user_runner.base_runner.token}"
    executor = "shell"

  CONTENT
}
