#!/bin/bash

USER="ansible"
HOST="ip address of remote VM"

ping_result=$(ping -c 1 $HOST)

if [[ $ping_result == *"1 packets transmitted, 1 received"* ]]; then
    echo "The server with IP $HOST is up and running"
else
    echo "The server with IP $HOST is down"
    exit 1
fi

if ssh -q -o ConnectTimeout=5 -o StrictHostKeyChecking=no "$USER@$HOST" exit; then
    echo "Server is reachable."
else
    echo "Server is not reachable."
fi


echo "healthchecking completed"
