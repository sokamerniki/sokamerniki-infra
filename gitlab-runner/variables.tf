variable "admin_username" {
  description = "VM admins username"
  type        = string
  default     = "sokamernik"

}

variable "machine_size" {
  description = "VM  size"
  type        = string
  default     = "Standard_B2s"

}

variable "ssh_public_key_path" {
  description = "SSH public key"
  type        = string
  default     = "~/.ssh/devopsazurekey.pub"

}
variable "ssh_private_key_path" {
  description = "SSH public key"
  type        = string
  default     = "~/.ssh/devopsazurekey"

}

variable "resource_location" {
  description = "Resource Group Location"
  type        = string
  default     = "East Asia"

}
variable "gitlab_access_token" {
  description = "Gitlab Acces Token which is Personal"
  type        = string
  default     = "glpat-RMrZDm4ahxhxGsTFxGVz"
}

