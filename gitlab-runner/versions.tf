terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.6.0"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "gitlab" {
  token = var.gitlab_access_token
}