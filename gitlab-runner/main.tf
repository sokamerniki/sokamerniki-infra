

resource "azurerm_resource_group" "devops-rg" {
  name     = "devops-resources"
  location = var.resource_location
}


resource "azurerm_virtual_network" "devops-vn" {
  name     = "devops-network"
  location = azurerm_resource_group.devops-rg.location


  resource_group_name = azurerm_resource_group.devops-rg.name
  address_space       = ["10.0.0.0/16"]
  tags = {
    environment = "dev"
  }
}


resource "azurerm_subnet" "devops-subnet" {
  name                 = "devops-subnet"
  resource_group_name  = azurerm_resource_group.devops-rg.name
  virtual_network_name = azurerm_virtual_network.devops-vn.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_network_security_group" "devops-sg" {
  name                = "devops-sg"
  location            = azurerm_resource_group.devops-rg.location
  resource_group_name = azurerm_resource_group.devops-rg.name


  tags = {
    environment = "dev"
  }


}


resource "azurerm_network_security_rule" "devops-dev-rule" {
  name                        = "devops-dev-rule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.devops-rg.name
  network_security_group_name = azurerm_network_security_group.devops-sg.name
}




resource "azurerm_subnet_network_security_group_association" "devops-sga" {
  subnet_id                 = azurerm_subnet.devops-subnet.id
  network_security_group_id = azurerm_network_security_group.devops-sg.id


  depends_on = [azurerm_subnet.devops-subnet]
}




resource "azurerm_public_ip" "devops-ip" {
  name                = "devops-ip"
  resource_group_name = azurerm_resource_group.devops-rg.name
  location            = azurerm_resource_group.devops-rg.location
  allocation_method   = "Dynamic"


  tags = {
    environment = "dev"
  }
}




resource "azurerm_network_interface" "devops-nic" {
  name                = "devops-nic"
  location            = azurerm_resource_group.devops-rg.location
  resource_group_name = azurerm_resource_group.devops-rg.name




  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.devops-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.devops-ip.id


  }
  tags = {
    environment = "dev"
  }

}


resource "azurerm_linux_virtual_machine" "dev-vm" {
  name                = "dev-vm"
  resource_group_name = azurerm_resource_group.devops-rg.name
  location            = azurerm_resource_group.devops-rg.location
  size                = var.machine_size
  admin_username      = var.admin_username
  network_interface_ids = [
    azurerm_network_interface.devops-nic.id
  ]


  admin_ssh_key {
    username   = var.admin_username
    public_key = file(var.ssh_public_key_path)
  }


  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }


  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
  tags = {
    environment = "dev"
  }
}

module "nomad_deployer" {
  source = "./modules/nomad-deployer"
  
  vm_id  = azurerm_linux_virtual_machine.dev-vm.id
  vm_ip  = azurerm_linux_virtual_machine.dev-vm.public_ip_address
}

module "runner-registration" {
  source = "./modules/runner-registration"

  vm_id = azurerm_linux_virtual_machine.dev-vm.id
  vm_ip = azurerm_linux_virtual_machine.dev-vm.public_ip_address

}